from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.email_operator import EmailOperator
from airflow.operators import HelloOperator

def print_hello():
    return 'Hello world!'

default_args = {
        'owner': 'Yisus',
        'start_date':datetime(2019,11,23),
}

dag = DAG('Send_hello_world',
          schedule_interval=None,
          default_args = default_args, catchup=False)

print_operator = HelloOperator(task_id='print_operator', name='Yisus', dag=dag)

dummy_operator = DummyOperator(task_id='dummy_task', retries=1, dag=dag)

hello_operator = PythonOperator(task_id='hello_task', python_callable=print_hello, dag=dag)

email = EmailOperator(
        task_id='send_email',
        to='jesusphilipraiz@gmail.com',
        subject='Mail test',
        html_content=""" <h3>Hello World!</h3> """,
        dag=dag
)

join = DummyOperator(
    task_id='join',
    trigger_rule='one_success',
    dag=dag,
)

print_operator >> dummy_operator >> [hello_operator, email] >> join

# Tambien hay una funcion
# from airflow.utils.email import send_email
# ...
# send_email('you_email@address.com', title, body)