import os
import sys
import argparse

try:
    import json
except ImportError:
    import simplejson as json

class ExampleInventory(object):

    def __init__(self):
        self.inventory = {}
        self.read_cli_args()

        # Called with `--list`.
        if self.args.list:
            self.inventory = self.example_inventory()
        # Called with `--host [hostname]`.
        elif self.args.host:
            # Not implemented, since we return _meta info `--list`.
            self.inventory = self.empty_inventory()
        # If no groups or vars are present, return an empty inventory.
        else:
            self.inventory = self.empty_inventory()

        print json.dumps(self.inventory, indent = 4);


    def example_inventory(self):
        lines = [line.rstrip('\n') for line in open('data.txt')]
        # Myaddrip = '192.168.127.249'
        # Random = '192.168.127.145'
        User = lines[0]
        Password = lines[1]
        
        ListIp = [lines[elem] for elem in range(2, len(lines))]
        Myaddrip = ListIp[0]

        return {
            'group': {
                'hosts': [ip for ip in ListIp],
                'vars': {
                    'ansible_connection': 'ssh',
                    'ansible_ssh_user': User,
                    'ansible_ssh_pass':
                        Password
                }
            },
            '_meta': {
                'hostvars': {
                    Myaddrip: {
                        'host_specific_var': 'foo'
                    },
                    Myaddrip: {
                        'host_specific_var': 'bar'
                    }
                }
            }
        }

    # Empty inventory for testing.
    def empty_inventory(self):
        return {'_meta': {'hostvars': {}}}

    # Read the command line args passed to the script.
    def read_cli_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--list', action = 'store_true')
        parser.add_argument('--host', action = 'store')

        self.args = parser.parse_args()

ExampleInventory()